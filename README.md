# INSTRUCTIONS
I split the exercise in different task (features) and I create a branch for each feature that I was implemented.
In order to see the logical path that I follow you can checkout every branch starting from 01-initial-setup
and finishing with the last one 06-final-fixes.

You can also see bellow what I did in each branch



# ACTIONS MADE IN BRANCHES 

## 01-initial-setup
    added .gitignore (to explude node_modules and other files not needed in the repo)
    eslint add rule semicolon ";" at the end of statement (for extra saefty) 
    added .editconfig to have a standarize ide configuration (tabs and indentation) to match eslint
## 02-class-creation    
    we create a class for FraudRadar
    refactor test to use new instance of FraudRadar    
## 03-derived-classes
    * Create a class for loading "asyncronus" files. This will give us the advantage of separting the 
    logic of loading a file from the main class. 
    Also we will implement all error logic on this new class and finaly we are making the load of the
    file asyc for a better performance. In this particular case since we are using node > 7.5 
    we will get use of ASYNC / AWAIT pattern.
    
    * Do some refactoring on FraudRadar in order to use promises. We create a metod call doOtherStuff,
    that we will change leator in the future but for now it's ok in order to prove promise workflow
    
    * Change test to accept promises
    
    * Extract order as new class
    
    * Create Normalizer Class
    
    * Create a FraudRules Class: In this particular case we implement the engine FraudeRules functions to be chainable
    This wil bive us the advantage of using as many function as we want together and create any combination for fraud
    detection   
## 04-refactoring
    * refactor doOtherStuff function , spleat into static functions  
## 05-file-path
       * Receive the file path is not nice. Think and change the signature and make it cooler.
       * Mi idea is to store the filepath on an eviroment var to make it extra secure. 
       Doing so we have multiple advantages:
           -. We can have different enviroments with diferent end points (testing, pre, prod)
           -. We don't need to store the filepath on a repository, so nobody can steal it from the repo
           -. No need to create "if" clausules to test if we are in one enviroment or the other.
           -. We only need to setup an envirment variable call "FRAUD_PATH_DIR"
           
       * To be more efficient and mutiplatform I decide to use "dotenv" witch is a NPM module
       that has a zero-dependency module that loads environment variables from a .env file 
       into process.env. 
       Storing configuration in the environment separate from code is based on The Twelve-Factor App methodology.         
## 06-final-fixes
    * refator a bucle in fraudRules      
